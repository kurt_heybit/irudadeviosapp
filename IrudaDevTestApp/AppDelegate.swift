//
//  AppDelegate.swift
//  IrudaDevTestApp
//
//  Created by Kurt Seong on 18/02/2020.
//  Copyright © 2020 heybit. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        if Prefs.homeUrl.isEmpty {
            Prefs.homeUrl = "http://d12qd56mp1j02q.cloudfront.net/test/home"
        }
        
        return true
    }

}

