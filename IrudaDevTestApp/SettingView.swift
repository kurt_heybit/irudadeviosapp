//
//  SettingView.swift
//  IrudaDevTestApp
//
//  Created by Kurt Seong on 18/02/2020.
//  Copyright © 2020 heybit. All rights reserved.
//

import SwiftUI

struct SettingView: View {
    var homeUrl: Binding<String> { Binding (
        get: { Prefs.homeUrl },
        set: { value in Prefs.homeUrl = value }
        )
    }
    
    var token: Binding<String> { Binding (
        get: { Prefs.token },
        set: { value in Prefs.token = value }
        )
    }
    
    var body: some View {
        Form {
            Section(header: Text("Home Url")) {
                TextField(homeUrl.wrappedValue, text: homeUrl)
                    .keyboardType(.URL)
            }
            
            Section(header: Text("Token")) {
                TextField(token.wrappedValue, text: token)
            }
        }.navigationBarTitle("Settings")
    }
}

struct SettingView_Previews: PreviewProvider {
    static var previews: some View {
        SettingView()
    }
}

struct Prefs {
    static var homeUrl: String {
        get { UserDefaults.standard.string(forKey: "home_url") ?? "" }
        set(value) { UserDefaults.standard.set(value, forKey: "home_url") }
    }
    
    static var token: String {
        get { UserDefaults.standard.string(forKey: "token") ?? "" }
        set(value) { UserDefaults.standard.set(value, forKey: "token") }
    }
}
