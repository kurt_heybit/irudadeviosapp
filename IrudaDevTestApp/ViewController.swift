//
//  ViewController.swift
//  IrudaDevTestApp
//
//  Created by Kurt Seong on 18/02/2020.
//  Copyright © 2020 heybit. All rights reserved.
//

import UIKit
import WebKit
import SwiftUI
import TLDExtract
import AfriwanLib

class ViewController: UIViewController {
    @IBOutlet weak var webView: IrudaWebView! {
        didSet {
            WKWebView.enableLogging(LogginglevelAll)
        }
    }
    
    let extractor = try! TLDExtract()
    
    var rootDomain: String {
        guard let result: TLDResult = extractor.parse(Prefs.homeUrl) else { return "" }
        return result.rootDomain ?? ""
    }
    
    var baseUrl = "http://d12qd56mp1j02q.cloudfront.net"
    
    var viewModel: WebViewModel = WebViewModel(type: "root", path: "/test/bridge", title: "BridgeTest", callbackFunction: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        switch (viewModel.type) {
        case "root":
            let leftButton = UIBarButtonItem(title: "Home", style: .plain, target: self, action: #selector(home))
            self.navigationItem.leftBarButtonItem = leftButton
            
            let rightButton = UIBarButtonItem(title: "Settings", style: .plain, target: self, action: #selector(showSettings))
            self.navigationItem.rightBarButtonItem = rightButton
            
        case "dialog":
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(close))
        default: break
        }
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        preferences.javaScriptCanOpenWindowsAutomatically = true

        webView.configuration.preferences = preferences
        webView.allowsBackForwardNavigationGestures = true
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        disableZoom()
        
        webView.registerHandler("callNativeHandler") { (data, responseCallback) in
            dump(data)
            self.handleRequest(data: data, callbackFunction: responseCallback)
        }
        
        webView.callHandler("callJsHandler", data: ["something":"test"]) { a in
            
        }
        
        webView.load(baseUrl + viewModel.path)
        
        navigationItem.title = viewModel.title
    }
  
    @objc func showSettings(_ sender: Any) {
        let hostingController = UIHostingController(rootView: SettingView())
        self.navigationController?.pushViewController(hostingController, animated: true)
    }
    
    @objc func home(_ sender: Any) {
        webView.load(Prefs.homeUrl)
    }
    
    @objc func close(_ sender: Any) {
        dismiss(animated: true)
    }
    
    func handleRequest(data: Any, callbackFunction: @escaping WVJBResponseCallback) {
        guard let jsonObject = data as? [String: Any] else {
            return
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: [])
            let webCallRequest = try JSONDecoder().decode(WebCallRequest.self, from: jsonData)
            switch (webCallRequest.type) {
            case "pagePush":
                handlePagePush(data: webCallRequest.data, callbackFunction: callbackFunction)
                break
            case "pageDialog":
                handlePageDialog(data: webCallRequest.data, callbackFunction: callbackFunction)
                break
            case "pagePop":
                if navigationController?.popViewController(animated: true) == nil {
                    navigationController?.dismiss(animated: true)
                }
                sendResult(data: webCallRequest.data, callbackFunction: viewModel.callbackFunction)
                break
            case "pageReplace":
                handlePageReplace(data: webCallRequest.data, callbackFunction: callbackFunction)
                break
            case "toast":
                if let message = webCallRequest.data["message"] as? String {
                    toast(message: message, view: view)
                }
                break
                
            case "alert":
                handleAlert(data: webCallRequest.data, callbackFunction: callbackFunction)
                break
                
            case "setCookie":
                if let key = webCallRequest.data["key"] as? String, let value = webCallRequest.data["value"] as? String {
                    setCookie(domain: rootDomain, key: key, value: value)
                }
                break
            default: break
                
            }
        } catch let error {
            toast(message: error.localizedDescription, view: self.view)
        }
    }
    
    func handlePagePush(data: [String: Any], callbackFunction: @escaping WVJBResponseCallback) {
        dump(data)
        if let vc = R.storyboard.main.webViewController() {
            let viewModel = WebViewModel(type: "push", path: data["path"] as! String, title: data["title"] as! String, callbackFunction: callbackFunction)
            vc.viewModel = viewModel
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func handlePageDialog(data: [String: Any], callbackFunction: @escaping WVJBResponseCallback) {
        if let navVc = R.storyboard.main.rootViewController(), let webVc = navVc.topViewController as? ViewController {
            let viewModel = WebViewModel(type: "dialog", path: data["path"] as! String, title: data["title"] as! String, callbackFunction: callbackFunction)
            webVc.viewModel = viewModel
            present(navVc, animated: true)
        }
    }
    
    func handlePageReplace(data: [String: Any], callbackFunction: @escaping WVJBResponseCallback) {
        if let navVc = R.storyboard.main.rootViewController(), let webVc = navVc.topViewController as? ViewController {
            let viewModel = WebViewModel(type: "root", path: data["path"] as! String, title: data["title"] as! String, callbackFunction: callbackFunction)
            webVc.viewModel = viewModel
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController = navVc
        }
    }
    
    func handleAlert(data: [String: Any], callbackFunction: @escaping WVJBResponseCallback) {
        if let message = data["message"] as? String {
            var actions: [UIAlertAction] = []
            if let buttons = data["buttons"] as? [String: Any] {
                func makeAction(data: [String: Any], defaultLabel: String, style: UIAlertAction.Style) -> UIAlertAction {
                    let action = UIAlertAction(title: data["label"] as? String ?? defaultLabel, style: style) { _ in
                        self.sendResult(data: data["value"] as? [String: Any], callbackFunction: callbackFunction)
                    }
                    return action
                }
                
                if let negative = buttons["negative"] as? [String: Any] {
                    actions.append(makeAction(data: negative, defaultLabel: "취소", style: .cancel))
                }
                
                if let positive = buttons["positive"] as? [String: Any] {
                    actions.append(makeAction(data: positive, defaultLabel: "확인", style: .default))
                }
                
                
            }
            alert(title: message, message: nil, actions: actions, style: .alert)
        }
    }
    
    func sendResult(data: [String: Any]?, callbackFunction: WVJBResponseCallback?) {
        guard let data = data else {
            callbackFunction?("")
            return
        }
        let jsonData = try? JSONSerialization.data(withJSONObject: data, options: [])
        if let jsonData = jsonData, let jsonString = String(data: jsonData, encoding: .utf8) {
            callbackFunction?(jsonString)
        }
    }
    
    func setCookie(domain: String, key: String, value: String) {
        
        let cookie = HTTPCookie.init(properties: [
            .domain: ".\(domain)",
            .path: "/",
            .name: key,
            .value: value
        ])
        
        let cookieStore = WKWebsiteDataStore.default().httpCookieStore
        
        cookieStore.setCookie(cookie!)
        
    }
    
    func disableZoom() {
        let source: String = "var meta = document.createElement('meta');" +
        "meta.name = 'viewport';" +
        "meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';" +
        "var head = document.getElementsByTagName('head')[0];" +
        "head.appendChild(meta);"
        let script: WKUserScript = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        webView.configuration.userContentController.addUserScript(script)
    }
}

extension ViewController: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}

extension ViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let javascriptStyle = "var css = '*{-webkit-touch-callout:none;-webkit-user-select:none}'; var head = document.head || document.getElementsByTagName('head')[0]; var style = document.createElement('style'); style.type = 'text/css'; style.appendChild(document.createTextNode(css)); head.appendChild(style);"
        webView.evaluateJavaScript(javascriptStyle, completionHandler: nil)
    }
}

struct WebCallRequest: Decodable {
    enum CodingKeys: String, CodingKey {
        case type
        case data
    }
    let type: String
    let data: [String: Any]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        type = try container.decode(String.self, forKey: .type)
        data = try container.decode([String: Any].self, forKey: .data)
    }
}

struct WebViewModel {
    let type: String
    let path: String
    let title: String
    let callbackFunction: WVJBResponseCallback?
}

//extension WebCallRequest: Decodable {
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        type = try container.decode(String.self, forKey: .type)
//        data = try container.decode(Dictionary.self)
//    }
//}
