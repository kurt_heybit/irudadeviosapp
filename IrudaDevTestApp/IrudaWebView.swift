//
//  IrudaWebView.swift
//  IrudaDevTestApp
//
//  Created by Kurt Seong on 18/02/2020.
//  Copyright © 2020 heybit. All rights reserved.
//

import UIKit
import WebKit
import AfriwanLib

class IrudaWebView: WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        } else {
            toast(message: "Invalid URL: \(urlString)", view: self)
        }
    }
}
